(function () {
    'use strict';

    angular
        .module('rms')
        .controller('InitCtrl', InitCtrl);

    function InitCtrl($http) {
        var vm = this;

        vm.properties = [];
        vm.firstContact = [];
        vm.action = [];
        vm.occupancy = [];
        vm.selectedDate = new Date();
        vm.selectedProperty = {};
        vm.changeDate = changeDate;
        vm.changeProperty = changeProperty;
        vm.saveData = saveData;

        loadProperty();

        function loadProperty() {
            $http.get('/property')
                .then(function (res) {
                    vm.properties = res.data;
                }, function (err) {
                })
        }

        function changeDate() {
            loadData();
        }

        function changeProperty(data) {
            vm.selectedProperty = data;
            loadData();
        }

        function loadData() {
            vm.firstContact = [];
            vm.action = [];
            vm.occupancy = [];
            $http.get('/ajax', {
                params: {
                    date: vm.selectedDate,
                    property: vm.selectedProperty.id
                }
            }).then(function (res) {
                var sourceList = res.data.source;
                var firstContact = res.data.first_contact;
                var actionList = res.data.actions;
                var occupancyList = res.data.occupancy;
                angular.forEach(sourceList, function (source) {
                    var contact = {source: source};
                    var action = {source: source};
                    var occupancy = {source: source};
                    var contactIndex = firstContact.map(function (e) {return e.source_id}).indexOf(source.id);
                    var actionIndex = actionList.map(function (e) {return e.source_id}).indexOf(source.id);
                    var occupancyIndex = occupancyList.map(function (e) {return e.source_id}).indexOf(source.id);

                    if (contactIndex > -1) {
                        contact.calls = firstContact[contactIndex].calls;
                        contact.walk_ins = firstContact[contactIndex].walk_ins;
                        contact.emails = firstContact[contactIndex].emails;
                        contact.other = firstContact[contactIndex].other;
                        contact.unqualified = firstContact[contactIndex].unqualified;
                    }
                    vm.firstContact.push(contact);

                    if (actionIndex > -1) {
                        action.shown = actionList[actionIndex].shown;
                        action.applied = actionList[actionIndex].applied;
                        action.approved = actionList[actionIndex].approved;
                        action.movein_count = actionList[actionIndex].movein_count;
                        action.denied = actionList[actionIndex].denied;
                        action.cancels = actionList[actionIndex].cancels;
                        action.reapply = actionList[actionIndex].reapply;
                    }
                    vm.action.push(action);

                    if (occupancyIndex > -1) {
                        occupancy.total_apartments = occupancyList[occupancyIndex].total_apartments;
                        occupancy.vacant_apartments = occupancyList[occupancyIndex].vacant_apartments;
                        occupancy.renewals = occupancyList[occupancyIndex].renewals;
                        occupancy.pre_leased = occupancyList[occupancyIndex].pre_leased;
                    }
                    vm.occupancy.push(occupancy);
                });
            }, function (err) {
            });
        }

        function saveData() {
            var date = vm.selectedDate || new Date();
            var formData = {
                date: date.toISOString(),
                contact: JSON.stringify(vm.firstContact),
                action: JSON.stringify(vm.action),
                occupancy: JSON.stringify(vm.occupancy)
            };
            $http({
                method: 'POST',
                url: '/ajax/',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: formData
            }).then(function (res) {
                alert('Saved successfully');
            }, function (err) {
                alert('Failed to save');
            });
        }
    }
})();