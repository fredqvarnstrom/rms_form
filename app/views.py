import datetime
import json

from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView, View

from app.models import Action, CompanySource, FirstContact, Occupancy, Property

# Create your views here.


class DashboardPage(TemplateView):
    template_name = 'index.html'

    def dispatch(self, *args, **kwargs):
        return super(DashboardPage, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DashboardPage, self).get_context_data(**kwargs)
        if self.request.GET.get('date'):
            context['date'] = self.request.GET.get('date')
        else:
            context['date'] = datetime.date.today
        context['property'] = Property.objects.all().values()
        return context

    def get(self, request, *args, **kwargs):
        return render(request, 'index.html')
        # return super(DashboardPage, self).get(request, *args, **kwargs)


class DashboardAjaxView(View):

    def get(self, request, *args, **kwargs):
        response_dict = dict()
        query_data = request.GET
        if query_data.get('date'):
            selected_date = datetime.datetime.strptime(query_data.get('date'), '%Y-%m-%dT%H:%M:%S.%fZ')
        else:
            selected_date = datetime.datetime.today()

        company_list = CompanySource.objects\
            .filter(property_id=query_data.get('property'))\
            .values('id', 'name')
        first_contact = FirstContact.objects.filter(
            date=selected_date,
            source__in=company_list.values_list('id', flat=True))
        actions = Action.objects.filter(
            date=selected_date,
            source__in=company_list.values_list('id', flat=True))
        occpancy = Occupancy.objects.filter(
            date=selected_date,
            source__in=company_list.values_list('id', flat=True))

        response_dict['source'] = list(company_list)
        response_dict['first_contact'] = list(first_contact.values())
        response_dict['actions'] = list(actions.values())
        response_dict['occupancy'] = list(occpancy.values())

        return HttpResponse(json.dumps(response_dict, default=datetime_json_encoder))

    def post(self, request, *args, **kwargs):
        req_data = request.POST
        errors = []
        if req_data.get('date'):
            selected_date = datetime.datetime.strptime(req_data.get('date'), '%Y-%m-%dT%H:%M:%S.%fZ')
        else:
            selected_date = datetime.datetime.today()
        first_contact = json.loads(req_data.get('contact'))
        actions = json.loads(req_data.get('action'))
        occupancy = json.loads(req_data.get('occupancy'))

        for contact in first_contact:
            try:
                FirstContact.objects.update_or_create(
                    source_id=contact['source']['id'],
                    date=selected_date,
                    defaults={
                        "calls": contact.get('calls'),
                        "walk_ins": contact.get('walk_ins'),
                        "emails": contact.get('emails'),
                        "other": contact.get('other'),
                        "unqualified": contact.get('unqualified'),
                    }
                )
            except Exception as e:
                errors.extend(e)

        for item in actions:
            try:
                Action.objects.update_or_create(
                    source_id=item['source']['id'],
                    date=selected_date,
                    defaults={
                        "shown": item.get('shown'),
                        "applied": item.get('applied'),
                        "approved": item.get('approved'),
                        "movein_count": item.get('movein_count'),
                        "denied": item.get('denied'),
                        "cancels": item.get('cancels'),
                        "reapply": item.get('reapply'),
                    }
                )
            except Exception as e:
                errors.extend(e)

        for item in occupancy:
            try:
                Occupancy.objects.update_or_create(
                    source_id=item['source']['id'],
                    date=selected_date,
                    defaults={
                        "total_apartments": item.get('total_apartments'),
                        "vacant_apartments": item.get('vacant_apartments'),
                        "renewals": item.get('renewals'),
                        "pre_leased": item.get('pre_leased'),
                    }
                )
            except Exception as e:
                errors.extend(e)

        if errors:
            return HttpResponse(json.dumps(dict(state='Fail', error=errors)), content_type='application/json')
        return HttpResponse(json.dumps(dict(state='Success')), content_type='application/json')


class PropertyAjaxView(View):

    def get(self, request, *args, **kwargs):
        property_list = Property.objects.all().values()
        return HttpResponse(json.dumps(list(property_list)), content_type='application/json')


def datetime_json_encoder(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj
