import logging

from django.core.management.base import BaseCommand

from app.models import Property, CompanySource

LOG = logging.getLogger(__name__)

PROPERTY_LIST = [
    'The Moderne', 'The Verano', '1200 Bedford', 'Mayfair Square', 'Copper Square', 'The Novella',
    'Hotel Zero Degrees (Stamford, CT)', 'Hotel Zero Degrees (Norwalk, CT)', 'Delamar Southport', 'Parallel 41',
    'BLVD', 'Village at Maple Pointe', 'Village at River\'s Edge', 'Phoenix on Isaac', 'Marquis On The River',
    '100 Prospect', 'Hotel Zero Degrees (Danbury, CT)', 'Rippowam Place', 'Simeon Village'
]

COMPANY_LIST = [
    'Apartments.com', 'APT Guide', 'Craigslist', 'Drive-by', 'For Rent', 'Google', 'Novella Website',
    'Other', 'Phone', 'Realtor', 'Referral', 'Rent.com', 'RENTCafé', 'RMS Companies', 'RMS Rentals', 'Zillow'
]


class Command(BaseCommand):
    help = 'Initial data for Property and CompanySource'

    def handle(self, *args, **options):
        for property_name in PROPERTY_LIST:
            property_data, _ = Property.objects.update_or_create(name=property_name)
            for company_name in COMPANY_LIST:
                company_data, _ = CompanySource.objects.update_or_create(
                    property=property_data,
                    name=company_name
                )
