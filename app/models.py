import datetime

from django.db import models
from django.utils.translation import gettext as _


class Property(models.Model):
    name = models.CharField(max_length=50, default='')


class CompanySource(models.Model):
    property = models.ForeignKey(Property, null=True, blank=True)
    name = models.CharField(max_length=50, default='')

    class Meta:
        unique_together = ('property', 'name')


class FirstContact(models.Model):
    date = models.DateField(_("Date"), default=datetime.date.today)
    source = models.ForeignKey(CompanySource, null=True, blank=True)
    calls = models.IntegerField(default=0, null=True)
    walk_ins = models.IntegerField(default=0, null=True)
    emails = models.IntegerField(default=0, null=True)
    other = models.IntegerField(default=0, null=True)
    unqualified = models.IntegerField(default=0, null=True)

    class Meta:
        unique_together = ('source', 'date')


class Action(models.Model):
    date = models.DateField(_("Date"), default=datetime.date.today)
    source = models.ForeignKey(CompanySource, null=True, blank=True)
    shown = models.IntegerField(default=0, null=True)
    applied = models.IntegerField(default=0, null=True)
    approved = models.IntegerField(default=0, null=True)
    movein_count = models.IntegerField(default=0, null=True)
    denied = models.IntegerField(default=0, null=True)
    cancels = models.IntegerField(default=0, null=True)
    reapply = models.IntegerField(default=0, null=True)

    class Meta:
        unique_together = ('source', 'date')


class Occupancy(models.Model):
    date = models.DateField(_("Date"), default=datetime.date.today)
    source = models.ForeignKey(CompanySource, null=True, blank=True)
    total_apartments = models.IntegerField(default=0, null=True)
    vacant_apartments = models.IntegerField(default=0, null=True)
    renewals = models.IntegerField(default=0, null=True)
    pre_leased = models.IntegerField(default=0, null=True)

    class Meta:
        unique_together = ('source', 'date')
