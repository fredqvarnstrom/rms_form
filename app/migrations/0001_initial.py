# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-30 19:50
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(default=datetime.date.today, verbose_name='Date')),
                ('shown', models.IntegerField(default=0)),
                ('applied', models.IntegerField(default=0)),
                ('approved', models.IntegerField(default=0)),
                ('movein_count', models.IntegerField(default=0)),
                ('denied', models.IntegerField(default=0)),
                ('cancels', models.IntegerField(default=0)),
                ('reapply', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='CompanySource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='FirstContact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(default=datetime.date.today, verbose_name='Date')),
                ('calls', models.IntegerField(default=0)),
                ('walk_ins', models.IntegerField(default=0)),
                ('emails', models.IntegerField(default=0)),
                ('other', models.IntegerField(default=0)),
                ('unqualified', models.IntegerField(default=0)),
                ('source', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.CompanySource')),
            ],
        ),
        migrations.CreateModel(
            name='Occupancy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(default=datetime.date.today, verbose_name='Date')),
                ('total_apartments', models.IntegerField(default=0)),
                ('vacant_apartments', models.IntegerField(default=0)),
                ('renewals', models.IntegerField(default=0)),
                ('pre_leased', models.IntegerField(default=0)),
                ('source', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.CompanySource')),
            ],
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='companysource',
            name='property',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.Property'),
        ),
        migrations.AddField(
            model_name='action',
            name='source',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.CompanySource'),
        ),
        migrations.AlterUniqueTogether(
            name='occupancy',
            unique_together=set([('source', 'date')]),
        ),
        migrations.AlterUniqueTogether(
            name='firstcontact',
            unique_together=set([('source', 'date')]),
        ),
        migrations.AlterUniqueTogether(
            name='companysource',
            unique_together=set([('property', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='action',
            unique_together=set([('source', 'date')]),
        ),
    ]
