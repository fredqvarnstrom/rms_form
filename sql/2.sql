SELECT
  property.name AS property,
  s1.name       AS source
FROM
  (SELECT
     source_id,
     ifnull(sum(calls), 0) + ifnull(sum(walk_ins), 0) + ifnull(sum(emails), 0) + ifnull(sum(other), 0) +
     ifnull(sum(unqualified), 0) AS total,
     app_companysource.name,
     app_companysource.property_id
   FROM app_firstcontact
     INNER JOIN app_companysource ON app_firstcontact.source_id = app_companysource.id
   WHERE (app_firstcontact.date >= '{date_start}' and app_firstcontact.date < 'date_end')
   GROUP BY app_firstcontact.source_id) AS s1
  JOIN
  (SELECT
     s2.property_id,
     max(total) AS max_source
   FROM (SELECT
           source_id,
           ifnull(sum(calls), 0) + ifnull(sum(walk_ins), 0) + ifnull(sum(emails), 0) + ifnull(sum(other), 0) +
           ifnull(sum(unqualified), 0) AS total,
           app_companysource.property_id
         FROM app_firstcontact
           INNER JOIN app_companysource ON app_firstcontact.source_id = app_companysource.id
         WHERE (app_firstcontact.date >= '{date_start}' and app_firstcontact.date < 'date_end')
         GROUP BY app_firstcontact.source_id) AS s2
   GROUP BY property_id
  ) AS s3
    ON s1.property_id = s3.property_id AND s1.total = s3.max_source
  INNER JOIN
  (SELECT *

   FROM app_property
  ) AS property
    ON property.id = s1.property_id
WHERE total > 0;