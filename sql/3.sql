SELECT
  property.name,
  filtered1.`Total Apartments`                                                    AS total,
  filtered2.`Vacant Apartments`                                                   AS vacant,
  ROUND(100 - filtered2.`Vacant Apartments` / filtered1.`Total Apartments` * 100) AS 'Occupancy %'
FROM (SELECT
        filtered.property_id,
        sum(filtered.total_apartments) AS "Total Apartments"
      FROM
        (SELECT
           occupancy.*,
           app_companysource.property_id
         FROM app_occupancy AS occupancy
           INNER JOIN app_companysource ON occupancy.source_id = app_companysource.id
         WHERE (occupancy.date >= '{date_start}' AND occupancy.date < '{date_end}')
        )
          AS filtered
      GROUP BY filtered.property_id)
  AS filtered1
  INNER JOIN
  (SELECT
     filtered.property_id,
     sum(filtered.vacant_apartments) AS "Vacant Apartments"
   FROM
     (SELECT
        occupancy.*,
        app_companysource.property_id
      FROM app_occupancy AS occupancy
        INNER JOIN app_companysource ON occupancy.source_id = app_companysource.id
      WHERE (occupancy.date >= '{date_start}' AND occupancy.date < '{date_end}')
     )
       AS filtered
   GROUP BY filtered.property_id) AS filtered2
    ON filtered1.property_id = filtered2.property_id
  INNER JOIN
  (SELECT *

   FROM app_property
  ) AS property
    ON property.id = filtered1.property_id;
