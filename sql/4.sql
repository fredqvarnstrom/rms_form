SELECT
  property.name,
  filtered1.`Renewals`,
  filtered2.`Pre-leased`
FROM (SELECT
        filtered.property_id,
        sum(filtered.renewals) AS "Renewals"
      FROM
        (SELECT
           occupancy.*,
           app_companysource.property_id
         FROM app_occupancy AS occupancy
           INNER JOIN app_companysource ON occupancy.source_id = app_companysource.id
         WHERE (occupancy.date >= '{date_start}' AND occupancy.date < '{date_end}')
        )
          AS filtered
      GROUP BY filtered.property_id)
  AS filtered1
  INNER JOIN
  (SELECT
     filtered.property_id,
     sum(filtered.pre_leased) AS "Pre-leased"
   FROM
     (SELECT
        occupancy.*,
        app_companysource.property_id
      FROM app_occupancy AS occupancy
        INNER JOIN app_companysource ON occupancy.source_id = app_companysource.id
      WHERE (occupancy.date >= '{date_start}' AND occupancy.date < '{date_end}')
     )
       AS filtered
   GROUP BY filtered.property_id) AS filtered2
    ON filtered1.property_id = filtered2.property_id
  INNER JOIN
  (SELECT *

   FROM app_property
  ) AS property
    ON property.id = filtered1.property_id;
