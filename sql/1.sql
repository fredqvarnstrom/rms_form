SELECT
  property.name,
  filtered2.Inquiries                                          AS inquires,
  filtered1.`Total Applied`                                    AS applications,
  ROUND(filtered1.`Total Applied` / filtered2.Inquiries * 100) AS 'Conversion Rate'
FROM (SELECT
        filtered.property_id,
        sum(filtered.applied) AS "Total Applied"
      FROM
        (SELECT
           action.*,
           app_companysource.property_id
         FROM app_action AS action
           INNER JOIN app_companysource ON action.source_id = app_companysource.id
          WHERE (action.date >= '{date_start}' and action.date < '{date_end}')
        )
          AS filtered
      GROUP BY filtered.property_id)
  AS filtered1
  INNER JOIN
  (SELECT
     filtered.property_id,
     sum(filtered.RowSum) AS Inquiries
   FROM
     (SELECT
        app_companysource.property_id,
        ifnull(calls, 0) + ifnull(walk_ins, 0) + ifnull(emails, 0) + ifnull(other, 0) + ifnull(unqualified, 0) AS RowSum
      FROM app_firstcontact AS first
        INNER JOIN app_companysource ON first.source_id = app_companysource.id
       WHERE (first.date >= '{date_start}' and first.date < '{date_end}')
     )
       AS filtered
   GROUP BY filtered.property_id) AS filtered2
    ON filtered1.property_id = filtered2.property_id
  INNER JOIN
  (SELECT *

   FROM app_property
  ) AS property
    ON property.id = filtered1.property_id;