# Installation

- Clone source code and install packages

        cd /var/www
        mkdir vhosts
        cd vhosts
        git clone https://bitbucket.org/rpi_guru/rms_form

        cd rms_form
        sudo apt-get install python libpython-dev virtualenv

        virtualenv venv
        source venv/bin/activate

        pip install -r requirements.txt

- Configure mysql db

    Create a database named `rms` and create user: `rmsform/rmsform`

        python manage.py migrate
        python manage.py initial_data

- Configure apache2

        sudo cp conf/rmsform.conf /etc/apache2/sites-available/rmsform.conf
        sudo ln -s /etc/apache2/sites-available/rmsform.conf /etc/apache2/sites-enabled/rmsform.conf

        sudo chown www-data:www-data /var/www/vhosts/rms_form

        sudo service apache2 restart

> SQL Query

```
SELECT * FROM
	(SELECT * FROM app_firstcontact) AS first
	LEFT JOIN app_action AS action
		ON first.source_id = action.source_id
        AND first.date = action.date
	LEFT JOIN app_occupancy AS occupancy
		ON first.source_id = occupancy.source_id
        AND first.date = occupancy.date
INNER JOIN app_companysource ON (first.source_id = app_companysource.id)
WHERE (first.date = "2017-08-01" AND app_companysource.property_id = 2)
```